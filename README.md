# URL Shortener - React TypeScript Client

## Getting Started

- Up the backend API (it's in another repo)
- Run `npm start` to fire up this React app
- Run `npm test` to run all tests

## Todo List

- Add Redux
- Set up an e2e testing suite with either Cypress or Nightwatch
- Implement authentication e.g. Google Auth