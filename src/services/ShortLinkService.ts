import axios, { AxiosResponse } from 'axios';
import { ApiConstants } from '../constants/ApiConstants';

export class ShortLinkService {
  static async getAll(): Promise<AxiosResponse> {
    return await axios.get(`${ApiConstants.API_BASE_URL}/short-links`);
  }

  static async createOne(longLink: string): Promise<AxiosResponse> {
    const data = { longLink };

    return await axios.post(`${ApiConstants.API_BASE_URL}/short-links`, data);
  }

  static async getByShortUrl(shortUrl: string): Promise<AxiosResponse> {
    return await axios.get(`${ApiConstants.API_BASE_URL}/short-links/${shortUrl}`);
  }
}