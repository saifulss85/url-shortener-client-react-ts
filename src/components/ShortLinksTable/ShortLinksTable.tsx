import * as React from 'react';
import { ShortLink } from '../../models/ShortLink';
import { ShortLinksTableRow } from '../ShortLinksTableRow/ShortLinksTableRow';

interface Props {
  shortLinks: ShortLink[];
}

export const ShortLinksTable: React.FC<Props> = (props: Props) => {
  return (
    <table>
      <thead>
      <tr>
        <th>ID</th>
        <th>Short Link</th>
        <th>Long Link</th>
        <th>Num Clicks</th>
      </tr>
      </thead>

      <tbody>
      {props.shortLinks.map(shortLink => <ShortLinksTableRow shortLink={shortLink} key={shortLink.id} />)}
      </tbody>
    </table>
  );
};