import * as React from 'react';
import { Link } from 'react-router-dom';
import { ShortLink as ShortLinkPojo } from '../../models/ShortLink';

interface Props {
  shortLink: ShortLinkPojo;
}

export const ShortLinksTableRow: React.FC<Props> = (props: Props) => {
  return (
    <tr>
      <td>{props.shortLink.id}</td>
      <td><Link to={`/${props.shortLink.shortUrl}`}>{props.shortLink.shortUrl}</Link></td>
      <td>{props.shortLink.longUrl}</td>
      <td>{props.shortLink.clicks.length}</td>
    </tr>
  );
};