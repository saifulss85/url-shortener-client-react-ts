import * as React from 'react';

export const Error404: React.FC = () => {
  return (
    <div>
      Not found, baby.
    </div>
  );
};