import * as React from 'react';
import { Link } from 'react-router-dom';
import './AppBrand.css';

interface Props {
}

export const AppBrand: React.FC<Props> = (props: Props) => {
  return (
    <Link to="/" className="AppBrand brand-logo">
      <span className="hide-on-small-only">Awesome URL Shortener</span>
      <span className="hide-on-med-and-up" style={{ fontSize: '0.8em' }}>Awesome URL Shortener</span>
    </Link>
  );
};