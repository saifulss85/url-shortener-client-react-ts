import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { AuthView } from '../../views/base/AuthView';
import { CreateShortLink } from '../../views/CreateShortLink/CreateShortLink';
import { GoToLink } from '../../views/GoToLink/GoToLink';
import { Home } from '../../views/Home/Home';
import { ListShortLinks } from '../../views/ListShortLinks/ListShortLinks';
import { ViewShortLinkDetails } from '../../views/ViewShortLinkDetails/ViewShortLinkDetails';
import { Error404 } from '../Error404/Error404';

const App: React.FC = () => {
  return (
    <div id="App" className="container">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" render={() => <AuthView component={<Home />} />} />
          <Route exact path="/app" render={() => <AuthView component={<Home />} />} />
          <Route exact path="/app/short-links" render={() => <AuthView component={<ListShortLinks />} />} />
          <Route exact path="/app/short-links/create" render={() => <AuthView component={<CreateShortLink />} />} />
          <Route exact path="/app/short-links/:shortLinkId" render={() => <AuthView component={<ViewShortLinkDetails />} />} />
          <Route exact path="/([a-zA-Z0-9]{6})/" component={GoToLink} />
          <Route path="*" component={Error404} />
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default App;
