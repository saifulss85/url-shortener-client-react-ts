import * as React from 'react';
import { NavLink } from 'react-router-dom';
import { AppBrand } from '../AppBrand/AppBrand';
import './NavBar.css';

export const NavBar: React.FC = () => (
  <nav>
    <div className="nav-wrapper">
      <AppBrand />
      <ul id="nav-mobile" className="right hide-on-med-and-down">
        <li><NavLink exact to="/app/short-links/create">Create New Link</NavLink></li>
        <li><NavLink exact to="/app/short-links">View My Links</NavLink></li>
      </ul>
    </div>
  </nav>
);