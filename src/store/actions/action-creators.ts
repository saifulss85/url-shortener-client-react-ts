import { Dispatch } from 'redux';
import { ShortLinkFactory } from '../../factories/ShortLinkFactory';
import { ShortLinkService } from '../../services/ShortLinkService';
import { FETCH_SHORT_LINKS } from './action-types';

export const fetchShortLinks = () => async function (dispatch: Dispatch) {
  const response = await ShortLinkService.getAll();
  const shortLinks = ShortLinkFactory.createFromJsonArray(response.data);

  dispatch({
    type: FETCH_SHORT_LINKS,
    payload: shortLinks,
  });
};