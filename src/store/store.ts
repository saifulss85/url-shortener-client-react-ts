import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { ShortLink } from '../models/ShortLink';
import { rootReducer } from './reducers/reducers';

interface InitialState {
  shortLinks: ShortLink[],
}

export const initialState: InitialState = {
  shortLinks: [],
};

export const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk)),
);