import { FETCH_SHORT_LINKS } from '../actions/action-types';
import { initialState } from '../store';

interface Action {
  type: string,
  payload: any,
}

export const rootReducer = (state = initialState, action: Action) => {

  if (action.type === FETCH_SHORT_LINKS) {
    return Object.assign({}, state, {
      shortLinks: action.payload,
    });
  }

  return state;
};