import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store'
import { initialState } from '../../store/store';
import { ListShortLinks } from './ListShortLinks';
import thunk from 'redux-thunk';

it('renders without crashing', () => {
  const middleware = [thunk];
  const mockStore = configureStore(middleware);
  const store = mockStore(initialState);

  const div = document.createElement('div');
  ReactDOM.render(<Provider store={store}><ListShortLinks /></Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
