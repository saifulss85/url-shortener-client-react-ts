import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ShortLinksTable } from '../../components/ShortLinksTable/ShortLinksTable';
import { ShortLink as ShortLinkPojo } from '../../models/ShortLink';
import { fetchShortLinks } from '../../store/actions/action-creators';

interface Props {
  fetchShortLinks?: Function,
  shortLinks?: ShortLinkPojo[],
}

interface State {
}

class BaseListShortLinks extends Component<Props, State> {
  componentDidMount() {
    if (this.props.fetchShortLinks) this.props.fetchShortLinks();
  }

  render() {
    if (this.props.shortLinks) {
      const content = <ShortLinksTable shortLinks={this.props.shortLinks} />;

      return <div>{content}</div>;
    }

    return <div>this screen will show you a list of all the short links you created, in tabular form</div>;
  }
}

const mapStateToProps = (state: any): State => ({
  shortLinks: state.shortLinks,
});

const mapDispatchToProps = (dispatch: any): any => ({
  fetchShortLinks: () => dispatch(fetchShortLinks()),
});

export const ListShortLinks = connect(mapStateToProps, mapDispatchToProps)(BaseListShortLinks);