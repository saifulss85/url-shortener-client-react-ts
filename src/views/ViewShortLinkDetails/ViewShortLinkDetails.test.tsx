import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router';
import { ViewShortLinkDetails } from './ViewShortLinkDetails';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render((
    <MemoryRouter>
      <ViewShortLinkDetails />
    </MemoryRouter>
  ), div);
  ReactDOM.unmountComponentAtNode(div);
});
