import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

interface MatchParams {
  shortLinkId: string;
}

interface ViewShortLinkDetailsProps extends RouteComponentProps<MatchParams> {
}

const BaseViewShortLinkDetails = (props: ViewShortLinkDetailsProps) => {
  return (
    <div>
      ViewShortLinkDetails of {props.match.params.shortLinkId}
    </div>
  );
};

export const ViewShortLinkDetails = withRouter(BaseViewShortLinkDetails);