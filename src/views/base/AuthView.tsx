import React from 'react';
import { NavBar } from '../../components/NavBar/NavBar';

interface AuthViewProps {
  component: React.ReactElement,
}

export const AuthView = (props: AuthViewProps) => {
  return (
    <div>
      <NavBar />
      <div>{props.component}</div>
    </div>
  );
};