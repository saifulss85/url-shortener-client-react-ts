import { AxiosResponse } from 'axios';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ShortLinkFactory } from '../../factories/ShortLinkFactory';
import { ShortLinkService } from '../../services/ShortLinkService';
import { UrlUtils } from '../../utils/UrlUtils';

interface MatchParams {
  shortLinkUrl: string;

  [key: string]: string;
}

interface Props extends RouteComponentProps<MatchParams> {
}

interface State {
  longUrl: string | undefined;
  errorFetchingUrl: string | undefined;
}

class BaseGoToLink extends React.Component<Props, State> {
  state: State;

  constructor(props: Props) {
    super(props);

    this.state = {
      longUrl: undefined,
      errorFetchingUrl: undefined,
    };
  }

  async componentDidMount() {
    const shortUrl = this.props.match.params[0];

    let response: AxiosResponse<any>;
    try {
      response = await ShortLinkService.getByShortUrl(shortUrl);
      const shortLink = ShortLinkFactory.createFromJson(response.data);

      this.setState({ longUrl: shortLink.longUrl });
    } catch (e) {
      this.setState({ errorFetchingUrl: e.response.data.message });
    }
  }

  render() {
    if (this.state.errorFetchingUrl !== undefined) return <div>{this.state.errorFetchingUrl}</div>;
    if (this.state.longUrl === undefined) return <div>fetching link...</div>;

    window.location.replace(UrlUtils.addHttpIfNotPresent(this.state.longUrl));

    return null;
  }
}

export const GoToLink = (withRouter(BaseGoToLink));