import React from 'react';
import ReactDOM from 'react-dom';
import { CreateShortLink } from './CreateShortLink';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CreateShortLink />, div);
  ReactDOM.unmountComponentAtNode(div);
});
