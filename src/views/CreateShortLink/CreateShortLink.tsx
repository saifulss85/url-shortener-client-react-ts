import * as M from 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import React, { Component, SyntheticEvent } from 'react';
import { ShortLinkFactory } from '../../factories/ShortLinkFactory';
import { ShortLinkService } from '../../services/ShortLinkService';
import './CreateShortLink.css';

interface Props {

}

interface State {
  longLink: string
}

interface ValidationError {
  field: string,
  value: string,
  message: string,
}

function validateForm(longLink: string) {
  const errors: ValidationError[] = [];
  if (!longLink) errors.push({
    field: 'longLink',
    value: longLink,
    message: 'longLink cannot be empty'
  });

  return errors;
}

export class CreateShortLink extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      longLink: ''
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  async onSubmit(event: SyntheticEvent) {
    event.preventDefault();

    const errors = validateForm(this.state.longLink);

    if (errors.length > 0) {
      errors.forEach(error => {
        M.toast({
          html: `${error.message}`
        });
      });

      return;
    }

    const response = await ShortLinkService.createOne(this.state.longLink);
    const shortLink = ShortLinkFactory.createFromJson(response.data);

    M.toast({
      html: `Short link created for ${shortLink.longUrl} ${shortLink.shortUrl} with ID ${shortLink.id}`
    });
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col s12" style={{ marginTop: '16px' }}>
            <form onSubmit={this.onSubmit}>
              <input
                placeholder="Enter long link to shorten"
                onChange={event => this.setState({ longLink: event.target.value })}
                value={this.state.longLink}
              />
              <button type="submit" className='btn waves-effect waves-light'>Create!</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}