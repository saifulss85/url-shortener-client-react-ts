import React from 'react';
import { Link } from 'react-router-dom';
import './Home.css'

export const Home: React.FC = () => {
  return (
    <div className="Home valign-wrapper">

      <div className="row">
        <div className="col s12 Home__hero l8 offset-l2">
          <p className="Home__hero">xxx Because life's too short for long URLs</p>
          <div style={{
            textAlign: 'center'
          }}>
            <Link to="/app/short-links/create" className="Home__link">
              <button className="btn Home__button-1">
                Create New Link
              </button>
            </Link>

            <Link to="/app/short-links" className="Home__link">
              <button className="btn Home__button-2">
                View My Links
              </button>
            </Link>
          </div>
        </div>
      </div>

    </div>
  );
};