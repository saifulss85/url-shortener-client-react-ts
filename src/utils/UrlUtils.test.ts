import 'jest';
import { UrlUtils } from './UrlUtils';

describe('UrlUtils', () => {
  const nakedUrl = 'www.google.com';
  const protocolledUrl = 'http://www.google.com';

  it('exports a class called UrlUtils', () => {
    expect(UrlUtils).toBeDefined();
  });

  it('has a static function called addHttpIfNotPresent', () => {
    expect(UrlUtils.addHttpIfNotPresent).toBeDefined();
  });

  describe('addHttpIfNotPresent', () => {
    it('adds http:// to the string beginning if it does not start with http://', () => {
      expect(UrlUtils.addHttpIfNotPresent(nakedUrl)).toBe('http://www.google.com');
    });

    it('does nothing to the string if it already starts with http://', () => {
      expect(UrlUtils.addHttpIfNotPresent(protocolledUrl)).toBe('http://www.google.com');
    });
  });
});