export class UrlUtils {
  static addHttpIfNotPresent(url: string): string {
    if (!/^(?:f|ht)tps?:\/\//.test(url)) {
      url = "http://" + url;
    }

    return url;
  }
}