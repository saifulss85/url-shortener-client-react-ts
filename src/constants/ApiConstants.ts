const BASE_URL = process.env.REACT_APP_BACKEND_API_BASE_URL;

if (!BASE_URL) throw new Error('Looks like the .env file is not set');

export const ApiConstants = {
  BASE_URL,
  API_BASE_URL: `${BASE_URL}/api/v1`,
};