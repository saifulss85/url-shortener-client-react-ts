import { Click } from '../models/Click';

export interface ClickJson {
  id: number,
  short_link_id: number,
  client_remote_ip: string,
  created_at: string,
}

export class ClickFactory {
  static createFromJson(json: ClickJson) {
    return new Click(
      json.id,
      json.short_link_id,
      json.client_remote_ip,
      json.created_at,
    )
  }

  static createFromJsonArray(jsons: ClickJson[]) {
    if (!jsons) return [];

    return jsons.map(json => ClickFactory.createFromJson(json));
  }
}