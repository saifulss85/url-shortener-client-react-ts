import { ShortLink } from '../models/ShortLink';
import { ClickFactory, ClickJson } from './ClickFactory';

interface ShortLinkJson {
  id: number,
  short_url: string,
  long_url: string,
  clicks: ClickJson[],
  created_by: string,
  created_at: string,
  updated_at: string,
  deleted_at: string,
}

export class ShortLinkFactory {
  static createFromJson(json: ShortLinkJson) {
    return new ShortLink(
      json.id,
      json.short_url,
      json.long_url,
      ClickFactory.createFromJsonArray(json.clicks),
      json.created_by,
      json.created_at,
      json.updated_at,
      json.deleted_at,
    )
  }

  static createFromJsonArray(jsons: ShortLinkJson[]) {
    return jsons.map(json => ShortLinkFactory.createFromJson(json));
  }
}