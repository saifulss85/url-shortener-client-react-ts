export class Click {
  id: number;
  shortLinkId: number;
  clientRemoteIp: string;
  createdAt: string;

  constructor(
    id: number,
    shortLinkId: number,
    clientRemoteIp: string,
    createdAt: string
  ) {
    this.id = id;
    this.shortLinkId = shortLinkId;
    this.clientRemoteIp = clientRemoteIp;
    this.createdAt = createdAt;
  }
}