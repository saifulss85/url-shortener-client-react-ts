import { Click } from './Click';

export class ShortLink {
  id: number;
  shortUrl: string;
  longUrl: string;
  clicks: Click[] | [];
  createdBy: string;
  createdAt: string;
  updatedAt: string;
  deletedAt: string;

  constructor(
    id: number,
    shortUrl: string,
    longUrl: string,
    clicks: Click[] | [],
    createdBy: string,
    createdAt: string,
    updatedAt: string,
    deletedAt: string
  ) {
    this.id = id;
    this.shortUrl = shortUrl;
    this.longUrl = longUrl;
    this.clicks = clicks;
    this.createdBy = createdBy;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }
}