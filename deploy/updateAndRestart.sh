#!/usr/bin/env bash

# delete the entire cloned repo
sudo rm -rf /var/www/saifulshahril.com/urlshortener/url-shortener-client-react-ts

# cd into the clone location
cd /var/www/saifulshahril.com/urlshortener/

# clone the repo
sudo git clone https://gitlab.com/saifulss85/url-shortener-client-react-ts.git

# cd into the newly cloned repo
cd /var/www/saifulshahril.com/urlshortener/url-shortener-client-react-ts

# set the .env file
sudo cp .env.example .env
sudo bash -c "echo REACT_APP_BACKEND_API_BASE_URL=http://api.urlshortener.saifulshahril.com > .env"

sudo npm i
sudo npm run build
sudo systemctl restart nginx